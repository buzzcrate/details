# LibreCrate BoM

This is the Bill of Materials for LibreCrate, featuring LibreComputer LePotato 2GB.

## Table

| Description | Manufacturer | Store | Price (USD) | Quantity |
| ----------- | ------------ | ----- | ----- | ---- |
| GL.iNet Slate | [GL.iNet](https://www.gl-inet.com/products/gl-ar750s/) | [Amazon](https://www.amazon.com/gp/product/B07GBXMBQF) | $70 | 1 |
| Tenda SG105 | [Tenda](https://www.tendacn.com/en/product/sg105.html) | [Amazon](https://www.amazon.com/dp/B01K1JUE1E) | $12 | 1 |
| Jadaol Cat6 1 ft | Jadaol | [Amazon](https://www.amazon.com/gp/product/B01IQWGI0O) | $9 | 1 |
| Samsung EVO Select uSD | [Samsung](https://www.samsung.com/us/computing/memory-storage/memory-cards/microsdxc-evo-select-memory-card-w--adapter-128gb--2017-model--mb-me128ga-am/) | [Amazon](https://www.amazon.com/gp/product/B06XWZWYVP) | $21 | 4 |
| LibreComputer LePotato (2GB) | [LibreComputer](https://libre.computer/products/boards/aml-s905x-cc/) | [Amazon](https://www.amazon.com/gp/product/B074P6BNGZ) | $45 | 4 |
| LibreComputer heatsink | LibreComputer | [Amazon](https://www.amazon.com/dp/B078MCFM62) | $7 | 4 |
| LoveRPi cooling case | [LoveRPi](https://www.loverpi.com/collections/le-potato/products/loverpi-active-cooling-media-center-pc-case) | [Amazon](https://www.amazon.com/dp/B0792VGJHM/) | $8 | 4 |
| Anker 60W 6 port USB charger | [Anker](https://www.anker.com/store/powerport-6/A2123113) | [Amazon](https://www.amazon.com/gp/product/B00P936188/) | $33 | 1 |


## Notes

- The Anker power supply was selected for density & overall efficiency. If bought again, might consider [one with LCD](https://www.amazon.com/dp/B075V1HYFS) or [AmazonBasics with switch](https://www.amazon.com/dp/B0773J4N43).
- The SG105 switch was selected due to 5V, as everyting runs on over USB 2.0 tolerant voltages.
- The GL.iNet router _is_ more expensive than some options, but is very powerful & ships with OpenWRT.
- The heatsinks are _very strongly_ suggested.
