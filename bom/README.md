# Bill of Materials

A Bill of Materials includes all parts of the cluster, prices, and where you can purchase them.

When possible, ensure links to a global distributor and prices from there.

## Clusters

- [LibreCrate](libre.md)
- [HardCrate](hard.md)

