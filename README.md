[[_TOC_]]

# Summary

Documentation, explanations, and external references for **BuzzCrate: Bingo in a Box**

# Presentation

The original rendition of this talk was delievered at [SouthEast LinuxFest 2019][self].
Despite several technical issues with the intended content, Will & Jason managed
to deliver a decent amount of the content. This was [recorded on video][self-video],
and the slides [can be found on Google Slides][slides].

# Cluster Bill of Materials

We will do our best to keep these up to date, and don't expect them to change much.

- [LibreCrate](bom/libre.md)
- [HardCrate](https://docs.google.com/spreadsheets/d/1iV_Csf8fFONPQYtYJSadM3ju0fH--nKD5pII9HVM6zM/edit?usp=sharing)


# External links

Projects:

- [k3s](https://k3s.io)
- [RKE](https://github.com/rancher/rke)
- [Rancher](https://rancher.com/)

Linux Distributions:

- [Arch Linux ARM](http://archlinuxarm.org)

Single Board Computer:

- [LibreComputer](https://libre.computer)
- [Hardkernel](https://www.hardkernel.com)


# Talks, Slides, Repos

## [SouthEast LinuxFest](http://southeastlinuxfest.org/)
  * [SELF BuzzCrate 2019](https://www.youtube.com/watch?v=vRqG3NloJ6w)
  * [Slides SELF BuzzCrate](https://docs.google.com/presentation/d/1qf0CTkzLQUBdcTwNTpPjwaYeEqhc3VQQ-bPFO3sIqNI/edit)

## [Open Source Summit 2020](https://events.linuxfoundation.org/open-source-summit-north-america/)
  * [Public Video](https://youtu.be/bXLnFBNu6yI)
  * [Slides](https://docs.google.com/presentation/d/1mlbtZmvQs9hf5359Gp5-Qp-eIfA6LxXlZVUzKP_2Noc/edit?usp=sharing)

## [GitLab Commit KubeCon:DevOps 2021](https://gitlabcommitatkubeconna2021.sched.com/event/o3RZ/one-layer-builds-docker-not-included)
  * [Slides](https://docs.google.com/presentation/d/1UX8Q9lpuXqx6cwLSsUl0fMTmb-uFncpFTVaBuauKBVw/edit?usp=sharing)
  * [Example Project Demo](https://gitlab.com/buzzcrate/onelayer)
  * [Video](https://www.youtube.com/watch?v=iI7fqwNRa0Q)

# Live Demos
  
Demo for ARM fixed, metallb had the wrong IP's for my network since the last time I pushed. Links are live whenever the cluster is up below. 
* [Echo demo](http://staticserv.ddns.net:8001/test%20OSS%202020) *[repo](https://gitlab.com/buzzcrate/goecho)*
* [BookDemo](http://staticserv.ddns.net:8002/) *[repo](https://gitlab.com/buzzcrate/bookdemo)*
* [Chat Demo](http://staticserv.ddns.net:8003/) (hint: use `/nick <username>` to change username) *[repo](https://gitlab.com/buzzcrate/apps/buzzchat)*
